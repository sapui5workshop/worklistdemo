jQuery.sap.declare("com.demo.s276.WorklistDemo.Component");
sap.ui.getCore().loadLibrary("sap.ui.generic.app");
jQuery.sap.require("sap.ui.generic.app.AppComponent");

sap.ui.generic.app.AppComponent.extend("com.demo.s276.WorklistDemo.Component", {
	metadata: {
		"manifest": "json"
	}
});